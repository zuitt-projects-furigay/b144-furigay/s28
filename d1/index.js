

/*
	JAVASCRIPT SYNCHRONUS VS. ASYNCHRONOUS

		SYNCHRONUS

			In syncronuous operation tasks are performed one at a time and only when one is completed, the following is unblocked

		ASYNCHRONOUS	

			
		 
*/

// SYNCHRONUS JAVASCRIPT

console.log('Hello World')
// console.log('Hello Again')

for(let i = 0; i <=15; i++){
	console.log(i)
}
// Blocking is when the execution of additional js proocess must wait untill the operation completes.
// Blocking is just the code is slow.
console.log('Goodbye')


// ASYNCHRONOUS JAVASCRIPT

// we can classify asynchronous js operation with 2 pprimary triggers.

// 1. Browser API/WEB API events or function. These includes methods like setTimeout, or events handlers, like onclick, mouse over, scroll and many more.


// function printMe() {
// 	console.log('print Me')
// }

// setTimeout(printMe, 5000)

// another sample
// function print() {
// 	console.log('print meee')
// }

// function test() {
// 	console.log('test')
// }


// setTimeout(print, 4000)
// test()


// // ANother example

// function f1() {
// 	console.log('f1')
// }

// function f2() {
// 	console.log('f2')
// }

// function main() {
// 	console.log('main')

// 	setTimeout(f1 , 0)

// 	new Promise((resolve, reject) => resolve('I am a promise')).then(resolve => console.log(resolve))

// 	f2()
// }

// main();

// QUEUE (Data Structure)
// in java script, we have a queue of call back function, it is a called  a callback queue or task queue. A queue data structure is "First-in-First-out"

// JOB QUEUE
// every time a promise occurs in the code, the executor function gets into the job queue. The event loop works as usual, to look into the queues but gives priority to the job queue items over the callback queue items when the stack is free.


// 2. PROMISES
// A unique javascript object that allows us to perform asyncronous

// the promises object represents the eventual completion (or failure) of an synchronous operation and iits resulting value.


// new Promise((resolve, reject) => resolve('I am a promise')).then(resolve => console.log(resolve))

// ------------------------------------------------------------

/*
	REST API USING A jsonplaceholder

*/

// Getting all posts (GET method)

// Fetch - allows us to asynchronously request for resource(data)

// promise - we use the promised for async

/*
	SYNTAX:

		fetch('url', {optional objects})
		.then(respones => rsponse.json())
		.then(data)

Optional Objects - which contains additional information about our reqquests such the method, the body, and the headers of our request.
*/

console.log(fetch('https://jsonplaceholder.typicode.com/posts'))
	// a promimse may be in one of 3 possible states: pending, fulfilled or rejected.

fetch('https://jsonplaceholder.typicode.com/posts')
.then(response => response.json())/*parse the response as JSON*/
.then(data => {
	console.log(data)// process the results
}) 

// -----------------------------------------------

// async await function
async function fetchData(){
	// wait for the 'fetch' method to complete then stores the value in a variiable name.
	let result = await fetch('https://jsonplaceholder.typicode.com/posts')
	console.log(result)
	console.log(typeof result)

	// converts thhe data from the 'response'
	let json = await result.json()
	console.log(json)
	console.log(typeof json)
}

fetchData();

// ---------------------------------------------
// retrieving a specific post
fetch('https://jsonplaceholder.typicode.com/posts/1')
		.then(response => response.json()) 
		.then(data => {
			console.log(data)
		})


// ---------------------------------------------

// Crates a new post
fetch('https://jsonplaceholder.typicode.com/posts', {
	method: "POST",
	headers: {
		'Content-Type': 'application/json'
	},
	// Set the content/body of the 'request' object to be sent to the backend
	body: JSON.stringify({
		title: 'New Post',
		body: 'Hello Again',
		userId: 2
	})
})
.then(res => res.json())
.then(data => console.log(data))


// ----------------------------------------------
// updating a post

fetch('https://jsonplaceholder.typicode.com/posts/1', {
    method: 'PUT',
    headers: {
        'Content-Type': 'application/json'
    },
    body: JSON.stringify({
        title: 'New Post,',
        body: 'Hello Again',
        userID: 1
    })
})
.then(response => response.json())
.then(data => console.log(data))

// PUT method - updatesthe whole doocument. modifying resources where the client sends data that updates the enntire resources.

// PATCH method - partial update.



// ----------------------------------------------------
// delete a post

fetch('https://jsonplaceholder.typicode.com/posts/1', {method: 'DELETE'})
.then(res=> res.json())
.then(data=>console.log(data))


// ------------------------------------------------------
// filtering a post
// information sent via the url can be done by adding the question mark symbol(?).
/*
SYNTAX:
//Individual parameters
	//'url?parameterName=value'
//Multiple parameters
	//'url?parameterName=value&parameterB=valueB'

*/

fetch('https://jsonplaceholder.typicode.com/posts?userId=1')
.then(res => res.json())
.then(data => console.log(data))




// Retrieve a nested/related comments to posts.
// Retrieving comments for a specific post(/post/:id/comments)

fetch('https://jsonplaceholder.typicode.com/posts/2/comments')
.then(res => res.json())
.then(data => console.log(data))