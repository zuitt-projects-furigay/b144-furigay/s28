



console.log(fetch('https://jsonplaceholder.typicode.com/todos'))


// GET Method
fetch('https://jsonplaceholder.typicode.com/todos')
.then(response => response.json())
.then(data => {
	console.log(data)
})



// // MAP Method
// fetch('https://jsonplaceholder.typicode.com/todos?title')
// .then(res => res.json())
// .then(data => console.log(data))




// GET Retrieve Single
fetch('https://jsonplaceholder.typicode.com/todos/10')
.then(response => response.json())
.then(data => {
	console.log(data)
})




// #6-





// POST Method
fetch('https://jsonplaceholder.typicode.com/todos', {
	method: "POST",
	headers: {
		'Content-Type': 'application/json'
	},
	// Set the content/body of the 'request' object to be sent to the backend
	body: JSON.stringify({
		userId:2121,
		title: 'New Post',
		completed: true
	})
})
.then(res => res.json())
.then(data => console.log(data))




// UPDATE Method
fetch('https://jsonplaceholder.typicode.com/todos/10', {
    method: 'PUT',
    headers: {
        'Content-Type': 'application/json'
    },
    body: JSON.stringify({
       title: 'New Title',
       description: 'New Description',
       status: false,
       dateComplete: "date",
       userId: 201,
    })
})
.then(response => response.json())
.then(data => console.log(data))




// UPDATE Status and Date
fetch('https://jsonplaceholder.typicode.com/todos/10', {
    method: 'PUT',
    headers: {
        'Content-Type': 'application/json'
    },
    body: JSON.stringify({
       title: 'New Title',
       description: 'New Description',
       status: "complete",
       dateComplete: "12/02/2021",
       userId: 201,
    })
})
.then(response => response.json())
.then(data => console.log(data))



// DELETE Method
fetch('https://jsonplaceholder.typicode.com/todos/1', {method: 'DELETE'})
.then(res=> res.json())
.then(data=>console.log(data))



